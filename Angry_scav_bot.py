from config import BOT_PREFIX, DISCORD_TOKEN

import random

import discord
from discord.ext import commands

# Можно удалять, перекрестные ссылки вроде
from DataFetchers.AmmoFetcher import AmmoFetcher
from DataFetchers.ArmorFetcher import ArmorFetcher
from DataFetchers.WeaponFetcher import WeaponFetcher
from DataFetchers.ProvisionFetcher import ProvisionFetcher
from DataFetchers.LootFetcher import LootFetcher
from DataFetchers.MedicalFetcher import MedicalFetcher
from DataFetchers.HideoutFetcher import HideoutFetcher

from Models.Ammo import ammo_dict_to_embed
from Models.Armor import armor_dict_to_embed
from Models.Weapon import weapon_dict_to_embed
from Models.Provision import provision_dict_to_embed
from Models.Loot import loot_dict_to_embed
from Models.Medical import medical_dict_to_embed
from Models.Common import common_dict_to_embed
from Models.Hideout import module_dict_to_embed


ammo_fetcher = AmmoFetcher()
armor_fetcher = ArmorFetcher()
weapon_fetcher = WeaponFetcher()
provision_fetcher = ProvisionFetcher()
loot_fetcher = LootFetcher()
medical_fetcher = MedicalFetcher()
hideout_fetcher = HideoutFetcher()

ammo_list = ammo_fetcher.get_data()
armor_list = armor_fetcher.get_data()
weapon_list = weapon_fetcher.get_data()
provision_list = provision_fetcher.get_data()
loot_list = loot_fetcher.get_data()
medical_list = medical_fetcher.get_data()
hideout_dict = hideout_fetcher.get_data()

bot = commands.Bot(command_prefix=BOT_PREFIX)
bot.remove_command('help')

advices = [
    'Если использовать комбинацию для стрельбы за угол (ALT + S), то можно кинуть гранату (G) за угол',
    'Коэффициент распределения урона при убитой части тела следующие: 1 для ног, 1.5 для живота и 0.7 для рук',
    'Не стесняйтесь поворачиваться в бою боком к противнику, руки достаточно хорошо танкуют (По факту вы получаете гораздо меньше урона)',
    'Если у противника хорошая броня, а у вас хреновые патроны, стреляйте в ноги. Замедлите его, да и урон приличный нанесете',
    'Боссы и их прислужники тупят в проемах. Встаньте за ним и расстреливайте по одиночке, если возникли проблемы',
    'Хирургические комплекты по разному работают. Маленький комплект оставляет лишь 50-60% от максимального, в то время как большой 80-90%',
    'Голову и грудную клетку зашить нельзя. Так что если вам их выбили, следующий урон вас убьет',
    ('Гранаты имеют несколько поражающих эффектов. Первый - взрывная волна, второй - осколки.' +
     'От волны в упор броня не спасет, но на дистанции снизит урон, не повреждая броню'),
    'Если вашу группу заперли в комнате и кинули гранату, акт героизма в виде прикрытия телом гранаты может спасти остальную команду',
    'Боссы, прислужники и рейдеры имеют больше здоровья. В некоторых случаях это касается и головы. Поэтому охотится на них лучше с автоматическим огнем.',
    ('Штурман ходит около бревен перекрестка, угла крытого красного ангара, рядом с времянками и около тайника. Любит бегать, особенно если в него стреляют.' +
     'Убивать его стоит либо издалека, пока он не заметил тебя, или в ближнем из автомата. Учтите, что количество прислужников у него от 0 до 2'),
    ('Решала обитает на заправке или в одном из зданий общаги. У него 4 прислужника и их снаряжение может быть как дерьмовым, так и топовым.' +
     'Решала в случае боя прячется и начинает кемпить, в то время как остальные идут в лобовую.'),
    'Килла обитает около Кибы и Адика. Пока нет шума, сидит за углом, однако любит выбегать на стрельбу и кидать гранаты минометным образом.',
    'Боты не останавливают перевязку. Воспользуйтесь этим. Также светошумовые достаточно надолго притупляет их реакцию',
    ('А вы знали, что у каждого медикамента своя скорость применения и скорость лечения? Салева имеет большую скорость и объем лечения за применения в отличии от IFAK.' +
     'А бинт может оказаться полезней в скоротечном бою, чем обычные аптечки'),
    'Применяйте болеутоляющее перед вероятным боем. Лучше остаться без ноги и получать урон от бега, чем умереть из-за отсутствия возможности уйти с линии огня',
    'Кто же знал, что гранат m67 дальше всех летит и катится? 5 секунд задержки оказываются внезапно полезными',
    'Посчитал себя баскетбольным про? В прыжке граната летит гораздо левее и ниже обычного. Смотри, не вайпни команду.'
]


@bot.event
async def on_ready():
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print('------')


@bot.command()
async def say(ctx, arg):
    message = ' '.join(arg)
    if message == 'Вот он':
        await ctx.send('А ну иди сюда, ты говно жопа')
    else:
        await ctx.send(message)


def find_in_list(element, data_list, key='name'):
    return list(
        filter(lambda x: element.lower().strip() in x[key].lower(), data_list)
    )


def find_in_dict(element, data_list):
    return list(
        filter(lambda x: element.lower().strip() in x.lower(), data_list)
    )


@bot.command()
async def tip(ctx):
    await ctx.send(advices[random.randint(0, len(advices))])


@bot.command()
async def weapon(ctx, arg):
    answers = find_in_list(arg, weapon_list)

    if len(answers) == 0:
        await ctx.send('Cheeki Breeki, Haven\'t found, Не нашел ')
        return

    for answer in answers:
        await ctx.send(embed=weapon_dict_to_embed(answer))


@bot.command()
async def armor(ctx, arg):
    answers = find_in_list(arg, armor_list)

    if len(answers) == 0:
        await ctx.send('Cheeki Breeki, Haven\'t found, Не нашел ')
        return

    for answer in answers:
        await ctx.send(embed=armor_dict_to_embed(answer))


@bot.command()
async def ammo(ctx, arg):
    answers = find_in_list(arg, ammo_list)

    if len(answers) == 0:
        await ctx.send('Cheeki Breeki, Haven\'t found, Не нашел ')
        return

    for answer in answers:
        await ctx.send(embed=ammo_dict_to_embed(answer))


@bot.command()
async def food(ctx, arg):
    answers = find_in_list(arg, provision_list)

    if len(answers) == 0:
        await ctx.send('Cheeki Breeki, Haven\'t found, Не нашел ')
        return

    for answer in answers:
        await ctx.send(embed=provision_dict_to_embed(answer))


@bot.command()
async def loot(ctx, arg):
    answers = find_in_list(arg, loot_list)

    if len(answers) == 0:
        await ctx.send('Cheeki Breeki, Haven\'t found, Не нашел ')
        return

    for answer in answers:
        await ctx.send(embed=loot_dict_to_embed(answer))


@bot.command()
async def medical(ctx, arg):
    answers = find_in_list(arg, medical_list)

    if len(answers) == 0:
        await ctx.send('Cheeki Breeki, Haven\'t found, Не нашел ')
        return

    for answer in answers:
        await ctx.send(embed=medical_dict_to_embed(answer))


@bot.command()
async def habar(ctx, arg):
    habar_list = medical_list + loot_list + provision_list

    answers = find_in_list(arg, habar_list)

    if len(answers) == 0:
        await ctx.send('Не ну ты бы еще пустых консервных банок принес')
        return

    for answer in answers:
        await ctx.send(embed=common_dict_to_embed(answer))


@bot.command()
async def hideout(ctx, *args):
    if len(args) >= 1:
        answers = find_in_dict(args[0], hideout_dict)
        if len(answers) == 0:
            await ctx.send('Че, ня?')
            return
        level = 0 if len(args) == 1 else int(args[1])
        for answer in answers:
            if level != 0 and (level-1) in range(len(hideout_dict[answer])):
                await ctx.send(embed=module_dict_to_embed(hideout_dict[answer], level, answer))
            else:
                for i in range(len(hideout_dict[answer])):
                    await ctx.send(embed=module_dict_to_embed(hideout_dict[answer], i+1, answer))
    else:
        await ctx.send('Че, ня?')
        return


@bot.command()
async def help(ctx):
    embed = discord.Embed(title="Scav bot", description="Really slav bot for squating. List of commands are:", color=0x1b2e20)

    embed.add_field(name="$medical <arg>", value="выдает информацию о медикаменте(Дорабатывается)", inline=False)
    embed.add_field(name="$loot <arg>", value="выдает информацию по предмету бартера", inline=False)
    embed.add_field(name="$food <arg>", value="выдает информацию по еде", inline=False)
    embed.add_field(name="$weapon <arg>", value="выдает информацию по оружию", inline=False)
    embed.add_field(name="$armor <arg>", value="выдает информацию по броне", inline=False)
    embed.add_field(name="$ammo <arg>", value="выдает информацию по патрону(ам)", inline=False)
    embed.add_field(name="$say <arg>", value="работает попугаем", inline=False)
    embed.add_field(name="$habar <arg>", value="если вам прям лень вспоминать тип хабара и нужна лишь цена. Не включает в себя снаряжение", inline=False)
    embed.add_field(name="$tip", value="дает полезный совет", inline=False)

    await ctx.send(embed=embed)


bot.run(DISCORD_TOKEN)

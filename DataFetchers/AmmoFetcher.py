from DataFetchers import base


class AmmoFetcher(base.DataFetcher):
    def get_url(self):
        return "https://escapefromtarkov.gamepedia.com/Ballistics"

    def parse_html(self, soup):
        ammo_positions = {
            'name': 1,
            'damage': 2,
            'penetration': 3,
            'armor_damage': 4,
            'accuracy': 5,
            'recoil': 6,
            'fragmentation': 7,
            'armor1': 8,
            'armor2': 9,
            'armor3': 10,
            'armor4': 11,
            'armor5': 12,
            'armor6': 13,
        }
        ammo_positions_int = ['penetration', 'armor_damage',
                              'armor1', 'armor2', 'armor3', 'armor4', 'armor5', 'armor6']

        tables_temp = []

        def parse_catridges(soup_page):
            catridges = []

            for h2 in soup.select('table tbody'):
                tables_temp.append(h2)

            act_table = tables_temp[1]

            for tr in act_table.select('tr')[3:]:
                first_td = tr.select_one('td')
                rowspan = first_td.get('rowspan')

                catridge = dict()

                for key, pos in ammo_positions.items():
                    position = pos if rowspan == '1' else pos + 1
                    selector = 'td:nth-child({})'.format(position)
                    value = base.escape_text(tr.select_one(selector).text)
                    catridge[key] = int(
                        value) if key in ammo_positions_int else value

                catridge['caliber'] = base.escape_text(first_td.text)
                catridges.append(catridge)

            return catridges

        return parse_catridges(soup)

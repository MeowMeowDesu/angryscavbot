from DataFetchers import base


class ArmorFetcher(base.DataFetcher):
    def get_url(self):
        return "https://escapefromtarkov.gamepedia.com/Armor_vests"

    def parse_html(self, soup):
        armor_positions = {
            'name': 2,
            'class': 3,
            'zones': 4,
            'durability': 5,
            'movement': 6,
            'turn_speed': 7,
            'ergonomics': 8
        }
        armor_int = ['class', 'ergonomics', 'durability']

        def parse_armor(soup):
            armor_temp = []
            armor_list = []

            for table in soup.select('table tbody'):
                armor_temp.append(table)
            act_table = armor_temp[0]

            for tr in act_table.select('tr')[1:]:
                armor = dict()
                armor['image'] = tr.select_one('img').get('src')

                for key, pos in armor_positions.items():
                    position = pos
                    selector = 'th:nth-child({0}),td:nth-child({0})'.format(position)
                    value = base.escape_text(tr.select_one(selector).text)
                    armor[key] = int(value) if key in armor_int else value

                armor_zones = armor['zones'].replace('and', ',').split(',')
                armor['zones'] = list(
                    map(lambda x: x.strip().lower(), armor_zones))
                armor_list.append(armor)

            return armor_list

        return parse_armor(soup)

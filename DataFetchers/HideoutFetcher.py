from DataFetchers import base


class HideoutFetcher(base.DataFetcher):
    def get_url(self):
        return "https://escapefromtarkov.gamepedia.com/Hideout"

    def parse_html(self, soup):

        def get_modules(soup):
            module_names = []
            h2 = soup.select('h2')[3]
            cursor = h2.find_next_sibling()
            target_table = cursor.select_one('div', {"class": "tabbertab"})
            modules_name = target_table.select('div')
            for module_name in modules_name:
                module_names.append(module_name['title'])
            return module_names

        def get_table_headers(soup):
            headers = []
            hideout_table = soup.select('table tbody')[4]
            headers_raw = hideout_table.select('tr')[0]
            for header in headers_raw.select('th'):
                headers.append(header.text.strip().lower())
            return headers
        module_names = get_modules(soup)
        headers = get_table_headers(soup)
        modules = soup.find_all('div', class_='tabbertab')
        name_iter = 0
        entire_modules = {}

        for module in modules:
            if name_iter >= len(module_names):
                break
            table = module.select('table tbody')
            module = []
            level = 1
            for tr in table[0].select('tr')[1:]:
                pos = 1
                module_level = {}
                for header in headers:
                    selector = 'th:nth-child({0}),td:nth-child({0})'.format(pos)
                    value = base.escape_text(tr.select_one(selector).text)
                    module_level[header] = value
                    pos += 1
                module_level['level'] = level
                module.append(module_level)
                level += 1

            entire_modules[module_names[name_iter]] = module
            name_iter += 1

        return entire_modules

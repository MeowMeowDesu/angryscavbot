from DataFetchers import base


class LootFetcher(base.DataFetcher):
    def get_url(self):
        return "https://escapefromtarkov.gamepedia.com/Loot"

    def parse_html(self, soup):
        loots = []
        headers = []
        loots_table = []
        headers_raw = soup.select('table tbody tr')[0]
        for header in headers_raw.select('th'):
            headers.append(header.text.strip().lower())
        for table in soup.select('table tbody'):
            loots_table.append(table)
        act_table = loots_table[0]
        for tr in act_table.select('tr')[1:]:
            pos = 0
            loot = {}
            for header in headers:
                selector = 'th:nth-child({0}),td:nth-child({0})'.format(pos+1)
                value = base.escape_text(tr.select_one(selector).text)
                pos += 1
                loot[header] = value
            loot['icon'] = tr.select_one('img').get('src')
            loots.append(loot)
        return loots

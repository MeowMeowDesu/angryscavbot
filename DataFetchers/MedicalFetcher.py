from DataFetchers import base

# ТУТ ДВЕ ТАБЛИЦЫ РАЗНЫЕ. Форматнуть код надо бы


class MedicalFetcher(base.DataFetcher):
    def get_url(self):
        return "https://escapefromtarkov.gamepedia.com/Medical"

    def parse_html(self, soup):
        medicals = []
        headers = []
        medicals_table = []
        for table in soup.select('table tbody'):
            medicals_table.append(table)
        act_table = medicals_table[1]
        headers_raw = act_table.select('tr')[0]
        for header in headers_raw.select('th'):
            headers.append(header.text.strip().lower())
        for tr in act_table.select('tr')[1:]:
            pos = 0
            medical = {}
            for header in headers:
                selector = 'th:nth-child({0}),td:nth-child({0})'.format(pos+1)
                value = base.escape_text(tr.select_one(selector).text)
                pos += 1
                medical[header] = value
            medical['icon'] = tr.select_one('img').get('src')
            medicals.append(medical)

        headers = []
        act_table = medicals_table[2]
        headers_raw = act_table.select('tr')[0]
        for header in headers_raw.select('th'):
            headers.append(header.text.strip().lower())
        for tr in act_table.select('tr')[1:]:
            pos = 0
            medical = {}
            for header in headers:
                selector = 'th:nth-child({0}),td:nth-child({0})'.format(pos+1)
                value = base.escape_text(tr.select_one(selector).text)
                pos += 1
                medical[header] = value
            medical['icon'] = tr.select_one('img').get('src')
            medicals.append(medical)
        return medicals

import requests


class PricesFetcher(object):
    def __init__(self):
        self.cookies = []

    def authorize(self):
        url = "https://tarkov-market.ru"
        response = requests.get(url)
        if response.status_code == 200:
            self.cookies = response.cookies

    def get_price(self, name: str, retries=3):
        if len(self.cookies) == 0:
            self.authorize()

        try:
            url = "https://tarkov-market.ru/api/items"

            response = requests.get(url, cookies=self.cookies)
            response.raise_for_status()

            data = response.json()
            item = next((item for item in data["items"] if item["wikiName"] == name), None)

            if item:
                price = item["avgDayPrice"]
                size = item["size"] if "size" in item else 1

                return (price, int(price / size))

            return 0, 0

        except requests.HTTPError as e:
            if retries == 0:
                return 0, 0

            if e.response.status_code == 401:
                self.authorize()

            return self.get_price(name, retries=retries-1)


fetcher = PricesFetcher()
get_price = fetcher.get_price

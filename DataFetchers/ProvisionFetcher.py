from DataFetchers import base


class ProvisionFetcher(base.DataFetcher):
    def get_url(self):
        return "https://escapefromtarkov.gamepedia.com/Provisions"

    def parse_html(self, soup):
        provisions = []
        headers = []
        provisions_table = []
        headers_raw = soup.select('table tbody tr')[0]
        for header in headers_raw.select('th'):
            headers.append(header.text.strip().lower())
        for table in soup.select('table tbody'):
            provisions_table.append(table)
        act_table = provisions_table[0]
        for tr in act_table.select('tr')[1:]:
            pos = 0
            provision = {}
            for header in headers:
                selector = 'th:nth-child({0}),td:nth-child({0})'.format(pos+1)
                value = base.escape_text(tr.select_one(selector).text)
                pos += 1
                provision[header] = value
            provision['icon'] = tr.select_one('img').get('src')
            provisions.append(provision)
        return provisions

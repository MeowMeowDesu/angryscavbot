from DataFetchers import base


class WeaponFetcher(base.DataFetcher):
    def get_url(self):
        return "https://escapefromtarkov.gamepedia.com/Weapons"

    def parse_html(self, soup):
        weapons = []

        valuable_h2 = []
        for h2 in soup.select('h2'):
            if h2.select('span.mw-headline:not(#Upcoming_weapons):not(#Unconfirmed_weapons)'):
                valuable_h2.append(h2)

        for h2 in valuable_h2:
            weapon_slot = h2.find('span').text
            cursor = h2.find_next_sibling()
            weapon_type = None

            while cursor.name != 'h2':
                if cursor.name == 'h3':
                    weapon_type = cursor.find(
                        'span').text if cursor.find('span') else None

                elif cursor.name == 'table':
                    weapon_tr_array = cursor.select('tbody > tr')

                    for weapon_tr in weapon_tr_array[1:]:
                        name_td = weapon_tr.select_one('td:nth-child(1)')
                        image = weapon_tr.select_one('img')
                        catridge_td = weapon_tr.select_one('td:nth-child(3)')
                        description_td = weapon_tr.select_one(
                            'td:nth-child(4)')

                        weapons.append({
                            'name': name_td.text.strip(),
                            'slot': weapon_slot,
                            'type': weapon_type,
                            'image': image.get('src'),
                            'catridge': catridge_td.text.strip(),
                            'description': description_td.text.strip() if description_td else None
                        })

                cursor = cursor.find_next_sibling()

        return weapons

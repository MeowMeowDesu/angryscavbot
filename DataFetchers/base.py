from bs4 import BeautifulSoup
from urllib import request
from urllib.parse import quote


class DataFetcher(object):
    @staticmethod
    def get_page_url(page_name):
        return "https://escapefromtarkov.gamepedia.com/{0}".format(quote(page_name))

    def get_url(self):
        raise NotImplementedError

    def load_html(self):
        html_page = request.urlopen(self.get_url())
        return BeautifulSoup(html_page, 'html.parser')

    def parse_html(self, soup):
        raise NotImplementedError

    def get_data(self):
        soup = self.load_html()
        return self.parse_html(soup)


def escape_text(text):
    return text.replace('*', '').strip()

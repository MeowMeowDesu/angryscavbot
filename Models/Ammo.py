from discord import Embed
from DataFetchers.base import DataFetcher
from DataFetchers.PricesFetcher import get_price


def ammo_dict_to_embed(ammo):
    price, slot_price = get_price(ammo['name'])

    recoil = ammo['recoil'] if ammo['recoil'] else 0
    accuracy = ammo['accuracy'] if ammo['accuracy'] else 0

    embed = Embed(
        title=ammo["name"],
        url=DataFetcher.get_page_url(ammo["name"])
    )

    embed.add_field(
        name="Damage",
        value='{}'.format(ammo['damage'])
    )

    embed.add_field(
        name="Penetration Value",
        value='{}'.format(ammo['penetration'])
    )

    embed.add_field(
        name="Accuracy",
        value='{}'.format(accuracy)
    )

    embed.add_field(
        name="Recoil",
        value='{}'.format(recoil)
    )

    embed.add_field(
        name="Fragmentation",
        value='{}'.format(ammo['fragmentation'])
    )

    embed.add_field(
        name="Armor damage",
        value='{}'.format(ammo['armor_damage'])
    )

    embed.add_field(
        name="Armor Penetration Effectiveness",
        value='{} / {} / {} / {} / {} / {}'.format(
            ammo['armor1'], ammo['armor2'], ammo['armor3'], ammo['armor4'], ammo['armor5'], ammo['armor6'])
    )

    if price:
        embed.add_field(
            name='Price',
            value="{0} ₽".format(price)
        )

    if slot_price:
        embed.add_field(
            name='Price per slot',
            value="{0} ₽".format(slot_price)
        )

    embed.set_footer(
        text="Ammo / {0}".format(ammo['name'].split()[0])
    )

    return embed

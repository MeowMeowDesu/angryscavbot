from discord import Embed
from DataFetchers.base import DataFetcher
from DataFetchers.PricesFetcher import get_price


def armor_dict_to_embed(armor):
    price, slot_price = get_price(armor['name'])

    embed = Embed(
        title=armor["name"],
        url=DataFetcher.get_page_url(armor["name"]),
    )

    embed.set_image(
        url=armor["image"]
    )

    embed.add_field(
        name="Class",
        value=armor["class"],
        inline=True
    )
    embed.add_field(
        name="Zones",
        value=', '.join(armor["zones"]),
        inline=True
    )

    embed.add_field(
        name="Durability penalty",
        value=armor["durability"],
        inline=False
    )
    embed.add_field(
        name="Ergonomics penalty",
        value=armor["ergonomics"],
        inline=False
    )

    embed.add_field(
        name="Movement penalty",
        value=armor["movement"],
        inline=True
    )

    embed.add_field(
        name="Turn speed penalty",
        value=armor["turn_speed"],
        inline=True
    )

    if price:
        embed.add_field(
            name='Price',
            value="{0} ₽".format(price)
        )

    if slot_price:
        embed.add_field(
            name='Price per slot',
            value="{0} ₽".format(slot_price)
        )

    embed.set_footer(
        text="Armor"
    )

    return embed

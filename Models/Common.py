# Чисто для эксперимента.
# Может имеет сделать базовый класс, в котором определен вывод цены и названия?

from discord import Embed
from DataFetchers.base import DataFetcher
from DataFetchers.PricesFetcher import get_price


def common_dict_to_embed(common):
    price, slot_price = get_price(common['name'])

    embed = Embed(
        title=common["name"],
        url=DataFetcher.get_page_url(common["name"]),
    )

    if price:
        embed.add_field(
            name='Price',
            value="{0} ₽".format(price)
        )

    if slot_price:
        embed.add_field(
            name='Price per slot',
            value="{0} ₽".format(slot_price)
        )

    embed.set_footer(
        text="Common"
    )

    return embed

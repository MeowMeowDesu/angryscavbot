from discord import Embed
from DataFetchers.base import DataFetcher


def module_dict_to_embed(module, level, name):
    embed = Embed(
        title=name,
        url=DataFetcher.get_page_url(name),
    )

    embed.add_field(
        name='Level',
        value=level
    )
    embed.add_field(
        name='Requirements',
        value=(module[level-1])['requirements']
    )
    embed.add_field(
        name='Function',
        value=(module[level-1])['function']
    )
    embed.add_field(
        name='Construction Time',
        value=(module[level-1])['construction time']
    )
    embed.set_footer(
        text="Убежище"+'/'+name
    )

    return embed

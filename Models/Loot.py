from discord import Embed
from DataFetchers.base import DataFetcher
from DataFetchers.PricesFetcher import get_price


# На сайте прям зашквар. То большие буквы, то нет. Думаю может перебором проходить потом
# Пушки или броню чекать вообще зашквар


def loot_dict_to_embed(loot):
    price, slot_price = get_price(loot['name'])

    embed = Embed(
        title=loot["name"],
        url=DataFetcher.get_page_url(loot["name"]),
        description=loot["notes"]
    )

    if price:
        embed.add_field(
            name='Price',
            value="{0} ₽".format(price)
        )

    if slot_price:
        embed.add_field(
            name='Price per slot',
            value="{0} ₽".format(slot_price)
        )

    embed.set_image(
        url=loot["icon"]
    )

    embed.add_field(
        name="Type",
        value=loot["type"],
        inline=True
    )

    embed.set_footer(
        text="ХАБАР"
    )

    return embed

from discord import Embed
from DataFetchers.base import DataFetcher
from DataFetchers.PricesFetcher import get_price


def medical_dict_to_embed(medical):
    price, slot_price = get_price(medical['name'])

    embed = Embed(
        title=medical["name"],
        url=DataFetcher.get_page_url(medical["name"]),
    )

    embed.set_image(
        url=medical["icon"]
    )

    embed.add_field(
        name='Type',
        value=medical['type']
    )

    effect = ''
    try:
        effect = medical['effect']
    except KeyError:
        effect = medical['buffs']+' '+medical['debuffs']

    embed.add_field(
        name='Effects',
        value=effect
    )

    if price:
        embed.add_field(
            name='Price',
            value="{0} ₽".format(price)
        )

    if slot_price:
        embed.add_field(
            name='Price per slot',
            value="{0} ₽".format(slot_price)
        )

    embed.set_footer(
        text="Наркомания"
    )

    return embed

from discord import Embed
from DataFetchers.base import DataFetcher
from DataFetchers.PricesFetcher import get_price


def provision_dict_to_embed(provision):
    price, slot_price = get_price(provision['name'])

    embed = Embed(
        title=provision["name"],
        url=DataFetcher.get_page_url(provision["name"]),
        description=provision["notes"]
    )

    embed.set_image(
        url=provision["icon"]
    )

    embed.add_field(
        name="Hydration",
        value=provision["hydration"],
        inline=True
    )

    embed.add_field(
        name="Energy",
        value=provision["energy"],
        inline=True
    )

    embed.add_field(
        name="Price",
        value=provision["energy"],
        inline=True
    )

    if price:
        embed.add_field(
            name='Price',
            value="{0} ₽".format(price)
        )

    if slot_price:
        embed.add_field(
            name='Price per slot',
            value="{0} ₽".format(slot_price)
        )

    embed.set_footer(
        text="Provision"
    )

    return embed

from discord import Embed
from DataFetchers.base import DataFetcher
from DataFetchers.PricesFetcher import get_price


def weapon_dict_to_embed(weapon):
    price, slot_price = get_price(weapon['name'])

    embed = Embed(
        title=weapon["name"],
        url=DataFetcher.get_page_url(weapon["name"]),
        description=weapon["description"],
    )

    embed.set_image(
        url=weapon["image"]
    )

    embed.add_field(
        name="Caliber",
        value=weapon["catridge"]
    )

    if price:
        embed.add_field(
            name='Price',
            value="{0} ₽".format(price)
        )

    if slot_price:
        embed.add_field(
            name='Price per slot',
            value="{0} ₽".format(slot_price)
        )

    embed.set_footer(
        text="Weapon / {0} / {1}".format(weapon["slot"], weapon["type"])
    )

    return embed
